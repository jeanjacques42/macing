import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)


export default new Vuex.Store({

  state: {
    marge: 20,
    marge2: 20,
    marge3: 20,
    total: 0,
    total2: 0,
    total3: 0,
    gain: 0,
    gain2: 0,
    gain3: 0,
    commission: 0,
    commission2: 0

  },
  mutations: {
    MARGE_UP(state, i) {
      if (i == "a") {
        state.marge++;
      }
      if (i == "b") {
        state.marge2++;
      }
      if (i == "c") {
        state.marge3++;
      }
    },
    MARGE_DOWN(state, i) {
      if (i == "a") {
        state.marge--;
      }
      if (i == "b") {
        state.marge2--;
      } if (i == "c") {
        state.marge3--;
      }
    },
    // INPUT(state) { 

    // }
  },
  actions: {
    margeUp({ commit }, i) {
      commit('MARGE_UP', i);
    },
    input({ commit }) {
      commit('INPUT');
    },
    margeDown({ commit }, i) {
      commit('MARGE_DOWN', i);
    }

  }
})